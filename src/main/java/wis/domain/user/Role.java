package wis.domain.user;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A Role.
 */
@Entity
@Table(name = "role")
@Where(clause = "deleted = 'false'")
public class Role implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "name", unique = true)
	@Size(min = 4, max = 25) // ROLE...
	private String name;

	@ManyToMany(mappedBy = "roles")
	@BatchSize(size = 20)
	private Set<User> users = new HashSet<>();

	@NotNull
	private Boolean deleted = false;

	@Version
	protected int version = 0;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public Role name(String name) {
		this.name = name;
		return this;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<User> getUsers() {
		return users;
	}

	public Role users(Set<User> users) {
		this.users = users;
		return this;
	}

	public Role addUser(User user) {
		this.users.add(user);
		user.getRoles().add(this);
		return this;
	}

	public Role removeUser(User user) {
		this.users.remove(user);
		user.getRoles().remove(this);
		return this;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public Role setDeleted(Boolean deleted) {
		this.deleted = deleted;
		return this;
	}

	public int getVersion() {
		return version;
	}

	public Role setVersion(int version) {
		this.version = version;
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Role role = (Role) o;
		if (role.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), role.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "Role{" + "id=" + getId() + ", name='" + getName() + "'" + "}";
	}
}

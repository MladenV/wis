package wis.domain.user;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Where;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * A User.
 */
@Entity
@Table(name = "user")
@Where(clause = "deleted = 'false'")
public class User implements Serializable, UserDetails {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "first_name")
	private String firstName;

	@NotNull
	@Column(name = "last_name")
	private String lastName;

	@NotNull
	@Column(name = "username")
	@Size(min = 2, max = 50)
	private String username;

	@NotNull
	@Column(name = "password")
	private String password;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "user_roles", joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"))
	@BatchSize(size = 20)
	private Set<Role> roles = new HashSet<>();

	@NotNull
	private Boolean deleted = false;

	@Version
	protected int version = 0;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public User firstName(String firstName) {
		this.firstName = firstName;
		return this;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public User lastName(String lastName) {
		this.lastName = lastName;
		return this;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUsername() {
		return username;
	}

	public User username(String username) {
		this.username = username;
		return this;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public User password(String password) {
		this.password = password;
		return this;
	}

	public void setPassword(String password) {
		this.password = password;
	}



	public Set<Role> getRoles() {
		return roles;
	}

	public User roles(Set<Role> roles) {
		this.roles = roles;
		return this;
	}

	public User addRoles(Role role) {
		this.roles.add(role);
		role.getUsers().add(this);
		return this;
	}

	public User removeRoles(Role role) {
		this.roles.remove(role);
		role.getUsers().remove(this);
		return this;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public User setDeleted(Boolean deleted) {
		this.deleted = deleted;
		return this;
	}

	public int getVersion() {
		return version;
	}

	public User setVersion(int version) {
		this.version = version;
		return this;
	}

	// UserDetails methods

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return roles.stream().map(authority -> new SimpleGrantedAuthority(authority.getName()))
				.collect(Collectors.toList());
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		User user = (User) o;
		if (user.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), user.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "User{" + "id=" + getId() + ", firstName='" + getFirstName() + "'" + ", lastName='" + getLastName() + "'"
				+ ", username='" + getUsername() + "'" + ", password='" + getPassword() + "'" + "}";
	}

}

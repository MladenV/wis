package wis.repository.user;

import wis.domain.user.User;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data JPA repository for the User entity.
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    @Query("select distinct user from User user left join fetch user.roles")
    List<User> findAllWithEagerRelationships(Pageable pageable);

    @Query("select user from User user left join fetch user.roles where user.id =:id")
    User findOneWithEagerRelationships(@Param("id") Long id);

    Optional<User> findUserByUsername(String username);

}

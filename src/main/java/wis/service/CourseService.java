package wis.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import wis.domain.Course;
import wis.repository.CourseRepository;

@Service
public class CourseService {

	@Autowired
	CourseRepository courseRepository;
	
	public List<Course> findAll(){
		return courseRepository.findAll();
	}
	
	public Page<Course> findAll(Pageable pageable){
		return courseRepository.findAll(pageable);
	};
	
	public Course findById(Long id) {
		return courseRepository.findById(id).orElse(null);
	}
	
	public Course save(Course course) {
		return courseRepository.save(course);
	}
	
	public boolean delete(Long id) {
		Course course = findById(id);
		if(course!=null) {
			courseRepository.delete(course);
			return true;
		}
		return false;
	}

}

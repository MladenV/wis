package wis.service.user;

import java.io.IOException;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import wis.domain.user.Role;
import wis.domain.user.User;
import wis.exception.InvalidUserException;
import wis.repository.user.UserRepository;
import wis.web.controller.dto.user.LoginDTO;

@Service
public class UserService {

	private final Logger log = LoggerFactory.getLogger(UserService.class);

	@Autowired
	private UserRepository repo;

	@Autowired
	private RoleService roleService;


	@Autowired
	private UserDetailsService userDetailsService;

	@Autowired
	AuthenticationManager authenticationManager;

	public Page<User> findAll(Pageable pageable) {
		log.debug("Request to all Users");
		return repo.findAll(pageable);
	}

	public User findOne(Long id) {
		log.debug("Request toUser : {}", id);
		return Optional.of(repo.findOneWithEagerRelationships(id))
				.orElseThrow(() -> new EntityNotFoundException("User not found"));
	}

	public User save(User user) {
		log.debug("Request to save User : {}", user);
		return repo.save(user);
	}

	public User findByUsername(String username) {
		log.debug("Request toUser : {}", username);
		return repo.findUserByUsername(username).orElseThrow(() -> new EntityNotFoundException("User not found"));

	}

	public User login(LoginDTO user) {
		UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(user.getUsername(),
				user.getPassword());
		authenticationManager.authenticate(token);
		User retVal = (User) userDetailsService.loadUserByUsername(user.getUsername());
		return retVal;
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void register(User user, String accountType) throws InvalidUserException, JSONException, IOException {

		if (repo.findUserByUsername(user.getUsername()).isPresent()) {
			throw new InvalidUserException();
		}

		// encode password
		user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));

		// TODO set user role based on the account type
		// currently anyone who registers is a teacher
		Role role = roleService.findByName("ROLE_TEACHER");
		user.addRoles(role);
		
		repo.save(user);

	}

	public User getLoggedInUser() {
		return (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	}
	
	public Optional<User> findUserByUsername(String username) {
		return repo.findUserByUsername(username);
	}
	
	
}

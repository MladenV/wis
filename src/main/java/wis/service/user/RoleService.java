package wis.service.user;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import wis.domain.user.Role;
import wis.repository.user.RoleRepository;

@Service
public class RoleService {

	private final Logger log = LoggerFactory.getLogger(RoleService.class);

	@Autowired
	private RoleRepository repo;

	public Page<Role> findAll(Pageable pageable) {
		log.debug("Request to all Roles");
		return repo.findAll(pageable);
	}

	public List<Role> findAll() {
		log.debug("Request to all Roles");
		return repo.findAll();
	}

	public Role findOne(Long id) {
		log.debug("Request toRole : {}", id);
		return repo.findById(id).orElseThrow(() -> new EntityNotFoundException("Role not found"));
	}

	public Role save(Role role) {
		log.debug("Request to save Role : {}", role);
		return repo.save(role);
	}

	public Role findByName(String name) {
		log.debug("Request toRole : {}", name);
		return repo.findByName(name).orElseThrow(() -> new EntityNotFoundException("Role not found"));
	}

}

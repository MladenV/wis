package wis.web.controller.mapper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import wis.domain.Course;
import wis.domain.CourseRealization;
import wis.web.controller.dto.CourseDTO;

@Component
public class CourseMapper implements Mapper<Course, CourseDTO>{

	@Autowired
	CourseRealizationMapper courseRealizationMapper;
	//TODO Sta sve treba da bude injektovano?
	
	public CourseDTO toDTO(Course course) {
		CourseDTO retVal = new CourseDTO();
		retVal.setDeleted(course.getDeleted());
		retVal.setEcts(course.getEcts());
		retVal.setId(course.getId());
		retVal.setNumberOfExcercises(course.getNumberOfExcercises());
		retVal.setNumberOfLectures(course.getNumberOfLectures());
		retVal.setObligatory(course.isObligatory());
		retVal.setTitle(course.getTitle());
		retVal.setVersion(course.getVersion());
		retVal.setCourseRealizations(new ArrayList<>());
		for (CourseRealization courseRealization : course.getCourseRealizations()) {
			retVal.getCourseRealizations().add("/api/course-realizations/"+courseRealization.getId());
		}
//		retVal.setCourseRealizations(courseRealizationMapper.toDTO(course.getCourseRealizations()));
		return retVal;
	}
	
	public Course toEntity(CourseDTO courseDTO) {
		// TODO implementirati logiku konverzije
		return null;		
	}
	
	public List<CourseDTO> toDTO(List<Course> courses){
		List<CourseDTO> retVal = new ArrayList<CourseDTO>();
		for (Course course : courses) {
			retVal.add(toDTO(course));
		}
		return retVal;
	}

	public List<Course> toEntity(List<CourseDTO> courseDTOs){
		// TODO implementirati logiku konverzije
		return null;
	}

}

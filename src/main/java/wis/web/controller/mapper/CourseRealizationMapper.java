package wis.web.controller.mapper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.stereotype.Component;

import wis.domain.CourseRealization;
import wis.web.controller.dto.CourseRealizationDTO;

@Component
public class CourseRealizationMapper {

	//@Autowired
	//TODO Sta sve treba da bude injektovano?

	public CourseRealizationDTO toDTO(CourseRealization courseRealization) {
		CourseRealizationDTO retVal = new CourseRealizationDTO();
		retVal.setDeleted(courseRealization.getDeleted());
		retVal.setEndDate(courseRealization.getEndDate());
		retVal.setId(courseRealization.getId());
		retVal.setStartDate(courseRealization.getStartDate());
		retVal.setVersion(courseRealization.getVersion());
		return retVal;
	}
	
	public CourseRealization toEntity(CourseRealizationDTO courseRealizationDTO) {
		// TODO implementirati logiku konverzije
		return null;		
	}
	
	public Collection<CourseRealizationDTO> toDTO(Collection<CourseRealization> courseRealizations){
		Collection<CourseRealizationDTO> retVal = new ArrayList<CourseRealizationDTO>();
		for (CourseRealization courseRealization : courseRealizations) {
			retVal.add(toDTO(courseRealization));
		}
		return retVal;
	}

	public List<CourseRealization> toEntity(List<CourseRealizationDTO> courseRealizationDTOs){
		// TODO implementirati logiku konverzije
		return null;
	}

}

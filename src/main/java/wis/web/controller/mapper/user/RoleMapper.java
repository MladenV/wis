package wis.web.controller.mapper.user;

import wis.domain.user.Role;
import wis.web.controller.dto.user.RoleDTO;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;

@Component
public class RoleMapper {

    public RoleDTO toDto(Role entity) {
        if ( entity == null ) {
            return null;
        }

        RoleDTO roleDTO = new RoleDTO();

        roleDTO.setId( entity.getId() );
        roleDTO.setName( entity.getName().substring(5).toLowerCase() );
        roleDTO.setVersion( entity.getVersion() );

        return roleDTO;
    }

    public Collection<Role> toEntity(Collection<RoleDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        Collection<Role> collection = new ArrayList<Role>( dtoList.size() );
        for ( RoleDTO roleDTO : dtoList ) {
            collection.add( toEntity( roleDTO ) );
        }

        return collection;
    }

    public Collection<RoleDTO> toDto(Collection<Role> entityList) {
        if ( entityList == null ) {
            return null;
        }

        Collection<RoleDTO> collection = new ArrayList<RoleDTO>( entityList.size() );
        for ( Role role : entityList ) {
            collection.add( toDto( role ) );
        }

        return collection;
    }

    public Role toEntity(RoleDTO roleDTO) {
        if ( roleDTO == null ) {
            return null;
        }

        Role role = new Role();

        role.setId( roleDTO.getId() );
        role.setName( roleDTO.getName() );
        role.setVersion( roleDTO.getVersion() );

        return role;
    }
}

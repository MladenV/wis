package wis.web.controller.mapper.user;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import wis.domain.user.Role;
import wis.domain.user.User;
import wis.exception.InvalidUserException;
import wis.service.user.RoleService;
import wis.service.user.UserService;
import wis.web.controller.dto.user.UserDTO;
import wis.web.controller.dto.user.UserRegisterDTO;


@Component
public class UserMapper {

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    public UserDTO toDto(User entity) {
        if ( entity == null ) {
            return null;
        }

        UserDTO userDTO = toDtoMinimal(entity);
        userDTO.setRoles(entity.getRoles().stream().map(r -> r.getId()).collect(Collectors.toSet()));
        return userDTO;
    }

    public UserDTO toDtoMinimal(User entity) {
        if ( entity == null ) {
            return null;
        }

        UserDTO userDTO = new UserDTO();

        userDTO.setId( entity.getId() );
        userDTO.setFirstName( entity.getFirstName() );
        userDTO.setLastName( entity.getLastName() );
        userDTO.setUsername( entity.getUsername() );
        userDTO.setVersion( entity.getVersion() );
        return userDTO;
    }

    public Collection<User> toEntity(Collection<UserDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        Collection<User> collection = new ArrayList<User>( dtoList.size() );
        for ( UserDTO userDTO : dtoList ) {
            collection.add( toEntity( userDTO ) );
        }

        return collection;
    }

    public Collection<UserDTO> toDto(Collection<User> entityList) {
        if ( entityList == null ) {
            return null;
        }

        Collection<UserDTO> collection = new ArrayList<UserDTO>( entityList.size() );
        for ( User user : entityList ) {
            collection.add( toDto( user ) );
        }

        return collection;
    }

    public Collection<UserDTO> toDtoMinimal(Collection<User> entityList) {
        if ( entityList == null ) {
            return null;
        }

        Collection<UserDTO> collection = new ArrayList<UserDTO>( entityList.size() );
        for ( User user : entityList ) {
            collection.add( toDtoMinimal( user ) );
        }

        return collection;
    }

    private User toEntity(UserDTO userDTO) {
        if ( userDTO == null ) {
            return null;
        }

        User user = new User();

        user.setId( userDTO.getId() );
        user.setFirstName( userDTO.getFirstName() );
        user.setLastName( userDTO.getLastName() );
        user.setUsername( userDTO.getUsername() );
        user.setVersion( userDTO.getVersion() );
        user.setRoles(userDTO.getRoles().stream().map(id -> roleService.findOne(id)).collect(Collectors.toSet()));

        return user;
    }
    public User toEntityWithoutPassword(UserDTO dto) {
        User user = toEntity(dto);
        User oldUser = userService.findByUsername(dto.getUsername());
        user.setPassword(oldUser.getPassword());
        return user;
    }

    @PreAuthorize("hasAnyRole('ADMINISTRATOR')")
    public User toEntityWithPassword(UserDTO dto) {
        User user = toEntity(dto);
        user.setPassword(new BCryptPasswordEncoder().encode(dto.getPassword()));
        return user;
    }
    
    public User toEntityForRegistration(UserRegisterDTO dto) throws InvalidUserException {
    	if ( dto == null ) {
            return null;
        }

        User user = new User();
        user.setFirstName(dto.getFirstName() );
        user.setLastName(dto.getLastName() );
        user.setUsername(dto.getUsername() );
        user.setPassword(dto.getPassword());
        
        return user;
        
    }
    
    public UserRegisterDTO toDtoForRegistration(User user) {
    	if (user == null) {
    		return null;
    	}
    	
    	UserRegisterDTO dto = new UserRegisterDTO();
    	dto.setFirstName(user.getFirstName());
    	dto.setLastName(user.getLastName());
    	dto.setUsername(user.getUsername());
    	// set accounttype string based on user role
    	dto.setAccountType(user.getRoles().stream()
    		.map(Role::getName)
    		.filter(n -> n.equals("ROLE_BORROWER"))
    		.map(n -> "KR")
    		.findAny()
    		.orElse("IN"));
    	
    	return dto;
    }
}

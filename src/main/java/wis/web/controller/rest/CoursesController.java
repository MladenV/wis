package wis.web.controller.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import wis.domain.Course;
import wis.service.CourseService;
import wis.web.controller.dto.CourseDTO;
import wis.web.controller.mapper.CourseMapper;

@RestController
@RequestMapping("api/courses")
public class CoursesController {

	@Autowired
	CourseService courseService;
	
	@Autowired
	CourseMapper courseMapper;

//	@GetMapping()
//	public Page<Course> findAll(Pageable pageable){
//		Page<Course> courses = courseService.findAll(pageable);
//		return courses;
//	}

	@GetMapping()
	public ResponseEntity<List<CourseDTO>> findAll(){
		List<Course> courses = courseService.findAll();
		return ResponseEntity.ok(courseMapper.toDTO(courses));
	}
	// /api/courses?title=Objektno&ects=6
	// /api/courses/123
	@GetMapping("/{id}")
	public CourseDTO findById(@PathVariable Long id){
		Course course = courseService.findById(id);
		return courseMapper.toDTO(course);
	}
	
	@PostMapping()
	public ResponseEntity<Course> addCourse(@RequestBody Course course) {
		if(course.getId()!=null&&course.getId()!=0) {
			return ResponseEntity.badRequest().build();
		}
		course  = courseService.save(course);
		return ResponseEntity.ok(course);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Course> updateCourse(@PathVariable long id, @RequestBody Course course){
		if(course.getId()==null||course.getId()==0) {
			return ResponseEntity.badRequest().build();
		}
		course = courseService.save(course);
		return ResponseEntity.ok(course);		
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Course> delete(@PathVariable long id){
		if(id==0) {
			return ResponseEntity.badRequest().build();
		}
		boolean deleted = courseService.delete(id);
		if(deleted) {
			return new ResponseEntity("Deleted",HttpStatus.OK);
		}
		else {
			return new ResponseEntity("The resource has already been deleted",HttpStatus.NOT_FOUND);
		}
	}

}

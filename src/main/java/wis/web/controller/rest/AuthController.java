package wis.web.controller.rest;

import java.io.IOException;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javassist.tools.web.BadHttpRequest;
import wis.domain.user.User;
import wis.exception.InvalidUserException;
import wis.security.TokenUtils;
import wis.service.user.UserService;
import wis.web.controller.dto.user.LoginDTO;
import wis.web.controller.dto.user.UserRegisterDTO;
import wis.web.controller.mapper.user.UserMapper;

@RestController
@RequestMapping("/api")
public class AuthController {

	@Autowired
	TokenUtils tokenUtils;

	@Autowired
	private UserMapper userMapper;

	@Autowired
	private UserService userService;

	@Value("${jwt.header}")
	private String header;

	@PostMapping("/login")
	public ResponseEntity login(@RequestBody final LoginDTO userDTO) {
		User user = userService.login(userDTO);
		HttpHeaders headers = new HttpHeaders();
		headers.set(header, tokenUtils.generateToken(user));
		return ResponseEntity.ok().headers(headers).build();
	}

	@PostMapping("/register")
	public ResponseEntity register(@RequestBody final UserRegisterDTO userRegister)
			throws InvalidUserException, JSONException, IOException {
		
		if (userService.findUserByUsername(userRegister.getUsername()).isPresent()) {
			return ResponseEntity.badRequest().body("Username taken");
		} else {
			User user = userMapper.toEntityForRegistration(userRegister);
			userService.register(user, userRegister.getAccountType());
			return ResponseEntity.ok(null);
		}
	}

}

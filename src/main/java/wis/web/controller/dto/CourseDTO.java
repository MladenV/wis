package wis.web.controller.dto;

import java.util.Collection;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class CourseDTO {

	@JacksonXmlProperty(isAttribute = true)
	private Long id;

	@JacksonXmlProperty(isAttribute = true)
	private Boolean deleted = false;

	private String title;
	
	private int ects;
	
	private boolean obligatory;
	
	private int numberOfLectures;

	private int numberOfExcercises;

	@JacksonXmlProperty(isAttribute = true)
	private int version = 0;

	private Collection<String> courseRealizations;
	
	
	
//	private Collection<CourseRealizationDTO> courseRealizations;
	
	//TODO implementirati vezu sa yearsOfStudy
	
//	public Collection<CourseRealizationDTO> getCourseRealizations() {
//		return courseRealizations;
//	}
//
//	public void setCourseRealizations(Collection<CourseRealizationDTO> courseRealizations) {
//		this.courseRealizations = courseRealizations;
//	}

	public Collection<String> getCourseRealizations() {
		return courseRealizations;
	}

	public void setCourseRealizations(Collection<String> courseRealizations) {
		this.courseRealizations = courseRealizations;
	}

	public CourseDTO() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getEcts() {
		return ects;
	}

	public void setEcts(int ects) {
		this.ects = ects;
	}

	public boolean isObligatory() {
		return obligatory;
	}

	public void setObligatory(boolean obligatory) {
		this.obligatory = obligatory;
	}

	public int getNumberOfLectures() {
		return numberOfLectures;
	}

	public void setNumberOfLectures(int numberOfLectures) {
		this.numberOfLectures = numberOfLectures;
	}

	public int getNumberOfExcercises() {
		return numberOfExcercises;
	}

	public void setNumberOfExcercises(int numberOfExcercises) {
		this.numberOfExcercises = numberOfExcercises;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}
	
}

package wis.web.controller.dto;

import java.time.LocalDate;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class CourseRealizationDTO {

	@JacksonXmlProperty(isAttribute = true)
	private Long id;

	@JacksonXmlProperty(isAttribute = true)
	private Boolean deleted = false;

	@JacksonXmlProperty(isAttribute = true)
	private int version = 0;

	private LocalDate startDate;
	
	private LocalDate endDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public CourseRealizationDTO() {
		super();
	}
	
	
}

package wis.web.controller.dto.user;

public class UserPasswordDTO {

    private String password;
    private String newPassword;

    public String getPassword() {
        return password;
    }

    public UserPasswordDTO setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public UserPasswordDTO setNewPassword(String newPassword) {
        this.newPassword = newPassword;
        return this;
    }
}

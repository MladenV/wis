package wis.web.controller.dto.user;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class RoleDTO {

    private Long id;
    private String name;

    protected int version;

    public Long getId() {
        return id;
    }

    public RoleDTO setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public RoleDTO setName(String name) {
        this.name = name;
        return this;
    }

    public int getVersion() {
        return version;
    }

    public RoleDTO setVersion(int version) {
        this.version = version;
        return this;
    }
}
